#!/bin/bash
ALB_NAME="$1"

#Get all necessary info about current settings
ALB_ARN=`aws elbv2 describe-load-balancers --query 'LoadBalancers[?LoadBalancerName==\`'$ALB_NAME'\`].LoadBalancerArn' --output text`
CURR_TG_ARN=`aws elbv2 describe-listeners --load-balancer-arn $ALB_ARN --query 'Listeners[?Port==\`443\`].DefaultActions[].TargetGroupArn' --output text`
VPC_ID=`aws elbv2 describe-load-balancers --query 'LoadBalancers[?LoadBalancerName==\`'$ALB_NAME'\`].VpcId' --output text`
CURR_TG_NAME=`aws elbv2 describe-target-groups --target-group-arns $CURR_TG_ARN --query 'TargetGroups[].TargetGroupName' --output text`
INSTANCE_ID=`aws elbv2 describe-target-health --target-group-arn $CURR_TG_ARN --query 'TargetHealthDescriptions[?TargetHealth.State==\`healthy\`].Target.Id' --output text`
ALB_LISTENER_ARN=`aws elbv2 describe-listeners --load-balancer-arn $ALB_ARN --query 'Listeners[?Port==\`443\`].ListenerArn' --output text`

#Create new temp Target Group
TEMP_TG_ARN=`aws elbv2 create-target-group --name "${CURR_TG_NAME}-maintenance" --protocol HTTP --port 80 --vpc-id $VPC_ID --health-check-protocol HTTP --health-check-port 80 --health-check-path "/index.html" --query 'TargetGroups[].TargetGroupArn' --output text`

#Register instances with new temp Target Group
aws elbv2 register-targets --target-group-arn $TEMP_TG_ARN --targets Id=${INSTANCE_ID}

#Modify ALB listener to use temp Target Group
aws elbv2 modify-listener --listener-arn $ALB_LISTENER_ARN --default-actions Type=forward,TargetGroupArn=${TEMP_TG_ARN}