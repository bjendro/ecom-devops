#!/bin/bash
ENVIRONMENT="$1"
DATE=`date +%m%d%Y`

echo "DATE = $DATE"

aws s3 cp s3://cd-deploy/$ENVIRONMENT/website/coborns-boot/target/ecomweb.war  s3://cd-deploy/$ENVIRONMENT/website/coborns-boot/backup/$DATE/ecomweb.war
aws s3 cp s3://cd-deploy/$ENVIRONMENT/admin/coborns-boot/target/ecomadmin.war  s3://cd-deploy/$ENVIRONMENT/admin/coborns-boot/backup/$DATE/ecomadmin.war
aws s3 cp s3://cd-deploy/$ENVIRONMENT/mobile/coborns-boot/target/ecommobileweb.war  s3://cd-deploy/$ENVIRONMENT/mobile/coborns-boot/backup/$DATE/ecommobileweb.war