#!/bin/bash
ASG="$1"
NUM="$2"

LAUNCH_CONFIG_NAME=`aws autoscaling describe-auto-scaling-groups --query 'AutoScalingGroups[?AutoScalingGroupName==\`'${ASG}'\`].LaunchConfigurationName' --output text`
aws autoscaling update-auto-scaling-group --auto-scaling-group-name ${ASG} --launch-configuration-name ${LAUNCH_CONFIG_NAME} --min-size $NUM --max-size $NUM --desired-capacity $NUM