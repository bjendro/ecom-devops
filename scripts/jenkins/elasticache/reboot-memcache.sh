#!/bin/bash
CLUSTER_ID="$1"

aws elasticache reboot-cache-cluster --cache-cluster-id $CLUSTER_ID --cache-node-ids-to-reboot 0001