#!/usr/bin/env bash

DAY=$(date +%d -d "91 days ago");
MONTH=$(date +%m -d "91 days ago");
YEAR=$(date +%Y -d "91 days ago");

echo "$DAY";
echo "$MONTH";
echo "$YEAR";

aws rds describe-db-snapshots --query 'DBSnapshots[?SnapshotCreateTime <= `'"$YEAR"'-'"$MONTH"'-'"$DAY"'T`][].DBSnapshotIdentifier' --output text > snapshots

file="snapshots"

while read line
do
 echo "$line"
 aws rds delete-db-snapshot --db-snapshot-identifier "$line"
done <"$file"