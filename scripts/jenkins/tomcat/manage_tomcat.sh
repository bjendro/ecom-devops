#!/bin/bash
ALB_NAME="$1"
ACTION="$2"
USR="jenkins"
KEY=/var/lib/jenkins/.ssh/id_rsa

#Get all necessary info
ALB_ARN=`aws elbv2 describe-load-balancers --query 'LoadBalancers[?LoadBalancerName==\`'$ALB_NAME'\`].LoadBalancerArn' --output text`
CURR_TG_ARN=`aws elbv2 describe-listeners --load-balancer-arn $ALB_ARN --query 'Listeners[?Port==\`443\`].DefaultActions[].TargetGroupArn' --output text`
INSTANCES_ID=`aws elbv2 describe-target-health --target-group-arn $CURR_TG_ARN --query 'TargetHealthDescriptions[].Target.Id' --output text`

#Stop/start tomcat on admin or mobile servers
for ID in $INSTANCES_ID
do

PRIVATE_IP=`aws ec2 describe-instances --instance-ids $ID --query 'Reservations[].Instances[].PrivateIpAddress' --output text`
ssh-keyscan $PRIVATE_IP >> ~/.ssh/known_hosts
echo "$ACTION tomcat on host $PRIVATE_IP"
sudo ssh -i $KEY ${USR}@${PRIVATE_IP} "sudo service tomcat8 $ACTION"

done