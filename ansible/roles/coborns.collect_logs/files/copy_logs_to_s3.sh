#! /bin/bash
instanceid=`curl http://169.254.169.254/latest/meta-data/instance-id`
awshostname="$(aws ec2 describe-instances --region us-east-1 --instance-id $instanceid --query 'Reservations[].Instances[].Tags[?Key==`Name`].Value[]' --output text)"
if [ -z $awshostname ]; then
exit 1
fi
find /var/log/tomcat8/ -mtime +7 -exec rm {} \;
find /var/log/tomcat8/ -mtime +1 -exec gzip {} \;
dest=s3://cdweb-logs/`date +%F`/$awshostname/$instanceid/
echo Copying to: "$dest"
aws s3 cp --recursive /var/log/tomcat8 $dest