#!/usr/bin/env bash
# Number of seconds
RANGE=900

number=$RANDOM
let "number %= $RANGE"

echo "Random value: $number"

sleep $number

echo "Restarting tomcat..."
sudo service tomcat8 restart
echo "Tomcat has been restarted"
sleep 10
date
echo "Tomcat status:"
sudo service tomcat8 status