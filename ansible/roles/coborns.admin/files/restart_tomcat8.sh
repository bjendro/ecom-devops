#!/usr/bin/env bash
echo "Restarting tomcat..."
sudo service tomcat8 restart
echo "Tomcat has been restarted"
sleep 10
date
echo "Tomcat status:"
sudo service tomcat8 status