#!/bin/bash
ALB_NAME="$1"

#Get all necessary ARNs
ALB_ARN=`aws elbv2 describe-load-balancers --query 'LoadBalancers[?LoadBalancerName==\`'$ALB_NAME'\`].LoadBalancerArn' --output text`
ALB_LISTENER_ARN=`aws elbv2  describe-listeners --load-balancer-arn $ALB_ARN --query 'Listeners[?Port==\`443\`].ListenerArn' --output text`
TEMP_TG_ARN=`aws elbv2  describe-listeners --load-balancer-arn $ALB_ARN --query 'Listeners[?Port==\`443\`].DefaultActions[].TargetGroupArn' --output text`

#Get all necessary TG names
TEMP_TG_NAME=`aws elbv2 describe-target-groups --target-group-arns $TEMP_TG_ARN --query 'TargetGroups[].TargetGroupName' --output text`
TG_NAME=${TEMP_TG_NAME%-*}

TG_ARN=`aws elbv2 describe-target-groups --query 'TargetGroups[?TargetGroupName==\`'$TG_NAME'\`].TargetGroupArn' --output text`

#Modify ALB Listener
aws elbv2 modify-listener --listener-arn $ALB_LISTENER_ARN --default-actions Type=forward,TargetGroupArn=${TG_ARN}

#Delete temp Target Group
aws elbv2 delete-target-group --target-group-arn $TEMP_TG_ARN