#!/bin/bash

# log gzipping

TMP_DIR="/mnt/s3"
BUCKET_LIST="s3://cdweb-logs"

LOGFILE=$TMP_DIR/$(basename $0).$(date +%s).processed.log


exec 200>/tmp/$(basename $0).lockfile;
flock -n 200 || exit 1;

function get_gzip_put_del()
{
  BUCKET=$1;
  FOLDER=$2;
  DIR_ON_S3=$BUCKET/$FOLDER;
  ARCHIVE=$TMP_DIR/tar-$FOLDER.tar.gz

  aws s3 cp --recursive $DIR_ON_S3 $TMP_DIR/$FOLDER
  tar -zcvf $ARCHIVE $TMP_DIR/$FOLDER
  aws s3 mv  $ARCHIVE $BUCKET/
  aws s3 rm --recursive $DIR_ON_S3
  aws s3 mv $BUCKET/tar-$FOLDER.tar.gz $BUCKET/archive/$FOLDER.tar.gz
  rm -f $ARCHIVE;
  rm -f -r $TMP_DIR/$FOLDER
  DATETIME=$(date +%F\ %T);
  echo "$DATETIME    $DIR_ON_S3.tar.gz" >> $LOGFILE
}

function task_runner()
{
  BUCK=$1;
  aws s3 ls $BUCK/ | grep -v 'gz$' | while read p
  do
    CREATED_DATE=$(echo $p | grep -Eo '[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}');
    CREATED_TIMESTAMP=$(date -d"$CREATED_DATE" +%s);
    OLDEST_THAN=`date -d"2 days ago" +%s`;
    if [[ $CREATED_TIMESTAMP -lt $OLDEST_THAN ]]; then
      DIR_NAME=$CREATED_DATE
     if [[ $DIR_NAME != "" ]]; then
          get_gzip_put_del $BUCK $DIR_NAME #&
      fi
    fi
  done
}


for b in $BUCKET_LIST
do
  task_runner $b;
done