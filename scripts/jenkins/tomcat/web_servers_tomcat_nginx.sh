#!/bin/bash
#Parameters
ASG="$1"
ACTION="$2"

#Variables
USR="jenkins"
KEY=/var/lib/jenkins/.ssh/id_rsa

#Get ids for all InService servers in AutoScaling group
INSTANCE_IDS=`aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name $ASG --query 'AutoScalingGroups[].Instances[?LifecycleState==\`InService\`].InstanceId' --output text`

#Restart nginx and stop/start tomcat on each server
for ID in $INSTANCE_IDS
do

PRIVATE_IP=`aws ec2 describe-instances --instance-ids $ID --query 'Reservations[].Instances[].PrivateIpAddress' --output text`
ssh-keyscan $PRIVATE_IP >> ~/.ssh/known_hosts
sudo ssh -i $KEY ${USR}@${PRIVATE_IP} "sudo service nginx restart"
sudo ssh -i $KEY ${USR}@${PRIVATE_IP} "sudo service tomcat8 $ACTION"

done