#!/usr/bin/env bash
DAY=$(date +%d -d "91 days ago");
MONTH=$(date +%m -d "91 days ago");
YEAR=$(date +%Y -d "91 days ago");

echo "${DAY}-${MONTH}-${YEAR}";

curl -XDELETE 'search-coborns-es-5ahe4zptafls3yfha5oys2pwwq.us-east-1.es.amazonaws.com/filebeat-'"$YEAR"'.'"$MONTH"'.'"$DAY"'?pretty'