#!/usr/bin/env bash
echo "Sophos AV will be restarted..."
sudo service sav-protect restart
echo "Sophos AV has been restarted"
sleep 10
date
echo "Sophos AV status:"
sudo service sav-protect status